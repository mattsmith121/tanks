﻿namespace TanksAnanlytics
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.pbHeatmap = new System.Windows.Forms.PictureBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lbPlayerTotalShots = new System.Windows.Forms.Label();
            this.lbAITotalShots = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.rbRound1 = new System.Windows.Forms.RadioButton();
            this.rbRound2 = new System.Windows.Forms.RadioButton();
            this.rbRound3 = new System.Windows.Forms.RadioButton();
            this.label3 = new System.Windows.Forms.Label();
            this.lbAIShots = new System.Windows.Forms.Label();
            this.lbPlayerShots = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lbDistanceAI = new System.Windows.Forms.Label();
            this.lbDistancePlayer = new System.Windows.Forms.Label();
            this.label99 = new System.Windows.Forms.Label();
            this.lbTime = new System.Windows.Forms.Label();
            this.tbData = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.lvHighscores = new System.Windows.Forms.ListView();
            ((System.ComponentModel.ISupportInitialize)(this.pbHeatmap)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.tbData.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // pbHeatmap
            // 
            this.pbHeatmap.BackColor = System.Drawing.Color.Gray;
            this.pbHeatmap.Location = new System.Drawing.Point(17, 33);
            this.pbHeatmap.Name = "pbHeatmap";
            this.pbHeatmap.Size = new System.Drawing.Size(325, 245);
            this.pbHeatmap.TabIndex = 0;
            this.pbHeatmap.TabStop = false;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(394, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // loadToolStripMenuItem
            // 
            this.loadToolStripMenuItem.Name = "loadToolStripMenuItem";
            this.loadToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
            this.loadToolStripMenuItem.Text = "Load";
            this.loadToolStripMenuItem.Click += new System.EventHandler(this.loadToolStripMenuItem_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(131, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Death Locations";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(17, 304);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(104, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Total Shots Fired";
            // 
            // lbPlayerTotalShots
            // 
            this.lbPlayerTotalShots.AutoSize = true;
            this.lbPlayerTotalShots.Location = new System.Drawing.Point(131, 304);
            this.lbPlayerTotalShots.Name = "lbPlayerTotalShots";
            this.lbPlayerTotalShots.Size = new System.Drawing.Size(48, 13);
            this.lbPlayerTotalShots.TabIndex = 4;
            this.lbPlayerTotalShots.Text = "Player: 0";
            // 
            // lbAITotalShots
            // 
            this.lbAITotalShots.AutoSize = true;
            this.lbAITotalShots.Location = new System.Drawing.Point(131, 331);
            this.lbAITotalShots.Name = "lbAITotalShots";
            this.lbAITotalShots.Size = new System.Drawing.Size(29, 13);
            this.lbAITotalShots.TabIndex = 5;
            this.lbAITotalShots.Text = "AI: 0";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(83, 331);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(0, 13);
            this.label5.TabIndex = 6;
            // 
            // rbRound1
            // 
            this.rbRound1.AutoSize = true;
            this.rbRound1.Location = new System.Drawing.Point(19, 360);
            this.rbRound1.Name = "rbRound1";
            this.rbRound1.Size = new System.Drawing.Size(66, 17);
            this.rbRound1.TabIndex = 7;
            this.rbRound1.TabStop = true;
            this.rbRound1.Text = "Round 1";
            this.rbRound1.UseVisualStyleBackColor = true;
            this.rbRound1.Click += new System.EventHandler(this.ChangeRound);
            // 
            // rbRound2
            // 
            this.rbRound2.AutoSize = true;
            this.rbRound2.Location = new System.Drawing.Point(112, 360);
            this.rbRound2.Name = "rbRound2";
            this.rbRound2.Size = new System.Drawing.Size(66, 17);
            this.rbRound2.TabIndex = 8;
            this.rbRound2.TabStop = true;
            this.rbRound2.Text = "Round 2";
            this.rbRound2.UseVisualStyleBackColor = true;
            this.rbRound2.Click += new System.EventHandler(this.ChangeRound);
            // 
            // rbRound3
            // 
            this.rbRound3.AutoSize = true;
            this.rbRound3.Location = new System.Drawing.Point(206, 360);
            this.rbRound3.Name = "rbRound3";
            this.rbRound3.Size = new System.Drawing.Size(66, 17);
            this.rbRound3.TabIndex = 9;
            this.rbRound3.TabStop = true;
            this.rbRound3.Text = "Round 3";
            this.rbRound3.UseVisualStyleBackColor = true;
            this.rbRound3.Click += new System.EventHandler(this.ChangeRound);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(14, 400);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(122, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Average Shots Fired";
            // 
            // lbAIShots
            // 
            this.lbAIShots.AutoSize = true;
            this.lbAIShots.Location = new System.Drawing.Point(149, 425);
            this.lbAIShots.Name = "lbAIShots";
            this.lbAIShots.Size = new System.Drawing.Size(29, 13);
            this.lbAIShots.TabIndex = 12;
            this.lbAIShots.Text = "AI: 0";
            // 
            // lbPlayerShots
            // 
            this.lbPlayerShots.AutoSize = true;
            this.lbPlayerShots.Location = new System.Drawing.Point(149, 399);
            this.lbPlayerShots.Name = "lbPlayerShots";
            this.lbPlayerShots.Size = new System.Drawing.Size(48, 13);
            this.lbPlayerShots.TabIndex = 11;
            this.lbPlayerShots.Text = "Player: 0";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(14, 457);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(165, 13);
            this.label4.TabIndex = 13;
            this.label4.Text = "Average Distance Travelled";
            // 
            // lbDistanceAI
            // 
            this.lbDistanceAI.AutoSize = true;
            this.lbDistanceAI.Location = new System.Drawing.Point(183, 480);
            this.lbDistanceAI.Name = "lbDistanceAI";
            this.lbDistanceAI.Size = new System.Drawing.Size(29, 13);
            this.lbDistanceAI.TabIndex = 15;
            this.lbDistanceAI.Text = "AI: 0";
            // 
            // lbDistancePlayer
            // 
            this.lbDistancePlayer.AutoSize = true;
            this.lbDistancePlayer.Location = new System.Drawing.Point(183, 457);
            this.lbDistancePlayer.Name = "lbDistancePlayer";
            this.lbDistancePlayer.Size = new System.Drawing.Size(48, 13);
            this.lbDistancePlayer.TabIndex = 14;
            this.lbDistancePlayer.Text = "Player: 0";
            // 
            // label99
            // 
            this.label99.AutoSize = true;
            this.label99.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label99.Location = new System.Drawing.Point(14, 511);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(130, 13);
            this.label99.TabIndex = 16;
            this.label99.Text = "Average Round Time:";
            // 
            // lbTime
            // 
            this.lbTime.AutoSize = true;
            this.lbTime.Location = new System.Drawing.Point(147, 511);
            this.lbTime.Name = "lbTime";
            this.lbTime.Size = new System.Drawing.Size(13, 13);
            this.lbTime.TabIndex = 17;
            this.lbTime.Text = "0";
            // 
            // tbData
            // 
            this.tbData.Controls.Add(this.tabPage1);
            this.tbData.Controls.Add(this.tabPage2);
            this.tbData.Location = new System.Drawing.Point(12, 28);
            this.tbData.Name = "tbData";
            this.tbData.SelectedIndex = 0;
            this.tbData.Size = new System.Drawing.Size(366, 558);
            this.tbData.TabIndex = 18;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.pbHeatmap);
            this.tabPage1.Controls.Add(this.lbTime);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.label99);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.lbDistanceAI);
            this.tabPage1.Controls.Add(this.lbPlayerTotalShots);
            this.tabPage1.Controls.Add(this.lbDistancePlayer);
            this.tabPage1.Controls.Add(this.lbAITotalShots);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.lbAIShots);
            this.tabPage1.Controls.Add(this.rbRound1);
            this.tabPage1.Controls.Add(this.lbPlayerShots);
            this.tabPage1.Controls.Add(this.rbRound2);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.rbRound3);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(358, 532);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Data";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.lvHighscores);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(358, 532);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Highscores";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // lvHighscores
            // 
            this.lvHighscores.HideSelection = false;
            this.lvHighscores.Location = new System.Drawing.Point(6, 6);
            this.lvHighscores.Name = "lvHighscores";
            this.lvHighscores.Size = new System.Drawing.Size(346, 520);
            this.lvHighscores.TabIndex = 0;
            this.lvHighscores.UseCompatibleStateImageBehavior = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(394, 598);
            this.Controls.Add(this.tbData);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Tank Analytics";
            ((System.ComponentModel.ISupportInitialize)(this.pbHeatmap)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.tbData.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pbHeatmap;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadToolStripMenuItem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lbPlayerTotalShots;
        private System.Windows.Forms.Label lbAITotalShots;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.RadioButton rbRound1;
        private System.Windows.Forms.RadioButton rbRound2;
        private System.Windows.Forms.RadioButton rbRound3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lbAIShots;
        private System.Windows.Forms.Label lbPlayerShots;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lbDistanceAI;
        private System.Windows.Forms.Label lbDistancePlayer;
        private System.Windows.Forms.Label label99;
        private System.Windows.Forms.Label lbTime;
        private System.Windows.Forms.TabControl tbData;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.ListView lvHighscores;
    }
}

