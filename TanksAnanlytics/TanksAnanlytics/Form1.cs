﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Data;
using System.Data.SQLite;
using System.Diagnostics;
using MongoDB.Driver;
using MongoDB.Bson.Serialization.Attributes;
using System.Drawing;

namespace TanksAnanlytics
{
    public partial class Form1 : Form
    {
        struct Vector2
        {
            public float x { get; }
            public float y { get; }

            public Vector2(float x, float y) {
                this.x = x;
                this.y = y;
            }
        }

        struct RoundData
        {
            public int playerShots { get; set; }
            public int aiShots { get; set; }
            public double time { get; set; }
            public float playerDistance { get; set; }
            public float aiDistance { get; set; }
            public int numRounds { get; set; }
        }

        private string databasePath;
        private DataTable roundData;
        private DataTable positionData;
        private DataTable shotsData;
        private RoundData round1Data;
        private RoundData round2Data;
        private RoundData round3Data;
        private const string ROUNDSTABLE = "RoundData";
        private const string POSITIONTABLE = "PositionDestroyed";
        private const string SHOTSTABLE = "ShotsFired";


        public Form1() {
            InitializeComponent();
            databasePath = "";
            roundData = new DataTable();
            positionData = new DataTable();
            shotsData = new DataTable();
            round1Data = new RoundData();
            round2Data = new RoundData();
            round3Data = new RoundData();
            MongoRead();
        }

        private void loadToolStripMenuItem_Click(object sender, EventArgs e) {
            using (OpenFileDialog openFileDialog = new OpenFileDialog()) {
                openFileDialog.InitialDirectory = "c:\\";
                openFileDialog.Filter = "database (*.db)|*.db|All files (*.*)|*.*";
                openFileDialog.RestoreDirectory = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK) {
                    //Get the path of specified file
                    databasePath = openFileDialog.FileName;
                }
            }

            if (databasePath != "") {
                LoadData();
                DisplayData();
            }
        }

        private void ChangeRound(object sender, EventArgs e) {
            RoundData roundData;
            if (rbRound1.Checked) {
                roundData = round1Data;
            } else if (rbRound2.Checked) {
                roundData = round2Data;
            } else { // Round 3
                roundData = round3Data;
            }

            if (roundData.numRounds == 0) {
                lbPlayerShots.Text = "Player: " + 0;
                lbAIShots.Text = "AI: " + 0;
                lbDistancePlayer.Text = "AI: " + 0;
                lbDistanceAI.Text = "AI: " + 0;
                lbTime.Text = "0";
                return;
            }

            lbPlayerShots.Text = "Player: " + ((double)roundData.playerShots / (double)roundData.numRounds);
            lbAIShots.Text = "AI: " + ((double)roundData.aiShots / (double)roundData.numRounds);
            lbDistancePlayer.Text = "Player: " + ((double)roundData.playerDistance / (double)roundData.numRounds);
            lbDistanceAI.Text = "AI: " + ((double)roundData.aiDistance / (double)roundData.numRounds);
            lbTime.Text = (roundData.time / (double)roundData.numRounds).ToString();

        }

        private void LoadData() {
            SQLRead(ROUNDSTABLE, ref roundData);
            SQLRead(SHOTSTABLE, ref shotsData);
            SQLRead(POSITIONTABLE, ref positionData);
        }

        private void SQLRead(string tableName, ref DataTable table) {
            using (SQLiteConnection dbConnection = new SQLiteConnection("Data Source=" + databasePath)) {
                dbConnection.Open();

                SQLiteDataAdapter adapter = new SQLiteDataAdapter("SELECT * from " + tableName, dbConnection);
                adapter.Fill(table);

                dbConnection.Close();
            }
        }

        private void DisplayData() {
            // Total Shots
            int playerTotalShots = 0;
            int aiTotalShots = 0;
            foreach (DataRow row in shotsData.Rows) {
                playerTotalShots += Int32.Parse(row["PlayerShots"].ToString());
                aiTotalShots += Int32.Parse(row["BotShots"].ToString());
            }

            lbPlayerTotalShots.Text = "Player: " + playerTotalShots;
            lbAITotalShots.Text = "AI: " + aiTotalShots;

            // Destroyed Positions
            List<Vector2> destroyed = new List<Vector2>();
            foreach (DataRow row in positionData.Rows) {
                float x = float.Parse(row["PosX"].ToString());
                // PosY is always ~0
                float y = float.Parse(row["PosZ"].ToString()); 
                destroyed.Add(new Vector2(x, y));
            }

            CreateHeatmap(destroyed);

            // Round Data
            foreach (DataRow row in roundData.Rows) {
                int round = Int32.Parse(row["Round"].ToString());
                if (round == 1) {
                    LoadRoundData(row, ref round1Data);
                } else if (round == 2) {
                    LoadRoundData(row, ref round2Data);
                } else { // Round 3
                    LoadRoundData(row, ref round3Data);
                }
            }

            // Update round display
            rbRound1.Checked = true;
            ChangeRound(null, null);
        }

        private void LoadRoundData(DataRow row, ref RoundData roundData) {
            roundData.playerShots = Int32.Parse(row["PlayerShots"].ToString());
            roundData.aiShots = Int32.Parse(row["BotShots"].ToString());
            roundData.playerDistance = float.Parse(row["PlayerDistance"].ToString());
            roundData.aiDistance = float.Parse(row["BotDistance"].ToString());
            roundData.time = double.Parse(row["Time"].ToString());
            roundData.numRounds++;
        }

        private void MongoRead() {
            MongoClient client = new MongoClient("mongodb+srv://GDAP-Student:jous.trok4POOS_pood@gdap2020-cluster.swxsa.mongodb.net/<dbname>?retryWrites=true&w=majority");
            var db = client.GetDatabase("GDAP_Exercise");
            List<Highscore> coll = db.GetCollection<Highscore>("Highscores").Find(_ => true).Sort("{ TotalRoundsWon: -1 }").ToList();
            SetupListView(coll);
        }

        private void SetupListView(List<Highscore> highscores) {
            // Set listview properties
            lvHighscores.View = View.Details;
            lvHighscores.Columns.Add("Name");
            lvHighscores.Columns.Add("Rounds Won").Width = -2;
            lvHighscores.Columns.Add("Accuracy").Width = -2;
            lvHighscores.GridLines = true;


            foreach (Highscore highscore in highscores) {
                string[] row = { highscore.Username, highscore.TotalRoundsWon.ToString(), highscore.Accuracy.ToString() };
                var listViewItem = new ListViewItem(row);
                lvHighscores.Items.Add(listViewItem);
                //Debug.WriteLine("{0}, {1}, {2}, {3}", highscore.id, highscore.Username, highscore.Accuracy, highscore.TotalRoundsWon);
            }

            // Resize name column
            lvHighscores.Columns[0].Width = -1;
        }

        private void CreateHeatmap(List<Vector2> data) {
            Color bleedColor = Color.FromArgb(30, 0, 0);
            Color pointColor = Color.FromArgb(100, 0, 0);
            Bitmap pic = new Bitmap(pbHeatmap.Width, pbHeatmap.Height);

            // Make pic black
            for (int x = 0; x < pic.Width; x++) {
                for (int y = 0; y < pic.Height; y++) {
                    pic.SetPixel(x, y, Color.Black);
                }
            }

            foreach (Vector2 pos in data){
                Vector2 mappedPos = MapToBitmap(pic.Width, pic.Height, pos);
                int x = (int)Math.Round(mappedPos.x, 0);
                int y = (int)Math.Round(mappedPos.y, 0);
                // bleed above
                if (y - 1 > 0) {
                    AddColour(ref pic, x, y - 1, bleedColor);
                }

                // bleed below
                if (pos.y + 1 < pic.Height) {
                    AddColour(ref pic, x, y + 1, bleedColor);
                }

                // bleed left
                if (x - 1 > 0) {
                    AddColour(ref pic, x - 1, y, bleedColor);
                }

                // bleed right
                if (x + 1 < pic.Width) {
                    AddColour(ref pic, x + 1, y, bleedColor);
                }

                AddColour(ref pic, x, y, pointColor);
            }

            pbHeatmap.Image = pic;
        }

        private Vector2 MapToBitmap(int bmWidth, int bmHeight, Vector2 pos) {
            // This is gonna be a bit hardcoded
            // Possible locations range from -50,-50 to 50,50
            // need to make them from 0,0 to bmWidth, bmHeight

            float x = pos.x;
            float y = pos.y;
            // Step 1 - add 45 to change -50,-50/50,50 to 0,0/100,100
            x += 50f;
            y += 50f;
            // Step 2 - change values to be between 0 - 1
            x = x / 100f;
            y = y / 100f;
            // Step 3 - multiply by bmWidth and bmHeight to get final coords
            x = x * bmWidth;
            y = y * bmHeight;

            // All done!
            return new Vector2(x, y);
        }

        private void AddColour(ref Bitmap pic, int x, int y, Color addColor) {
            // Get pixel colour
            Color color = pic.GetPixel(x, y);
            // Add colour
            int r = color.R + addColor.R;
            if (r > 255) {
                r = 255;
            }
            int g = color.G + addColor.G;
            if (g > 255) {
                g = 255;
            }
            int b = color.B + addColor.B;
            if (b > 255) {
                b = 255;
            }
            pic.SetPixel(x, y, Color.FromArgb(r, g, b));
        }
    }

    public class Highscore
    {
        [BsonId]
        public int id { get; set; }
        public string Username { get; set; }
        public float Accuracy { get; set; }
        public int TotalRoundsWon { get; set; }
    }
}
