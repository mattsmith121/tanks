﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor.PackageManager;
using UnityEngine;

public static class DataManager
{
    private static int m_PlayerShotsGame;
    private static int m_PlayerShotsRound;
    private static int m_PlayerHitsRound;
    private static int m_BotShotsGame;
    private static int m_BotShotsRound;
    private static Vector3 m_PlayerInitialPosition;
    private static Vector3 m_BotInitialPosition;
    private static Vector3 m_PlayerEndPosition;
    private static Vector3 m_BotEndPosition;
    private static double m_StartTime;
    private static double m_EndTime;

    public static void ShotFired(bool player) {
        if (player) {
            m_PlayerShotsRound++;
        } else {
            m_BotShotsRound++;
        }
    }

    public static void AIHit() {
        m_PlayerHitsRound++;
    }

    public static void SetTime(bool startTime, double time) {
        if (startTime) {
            m_StartTime = time;
        } else {
            m_EndTime = time;
        }
    }

    public static void SetPosition(bool initial, bool player, Vector3 position) {
        if (player) {
            if (initial) {
                m_PlayerInitialPosition = position;
            } else {
                m_PlayerEndPosition = position;
            }
            
        } else {
            if (initial) {
                m_BotInitialPosition = position;
            } else {
                m_BotEndPosition = position;
            }
        }
    }

    public static void RecordTankDeathPosition(Vector3 position) {
        SqliteManager.SQLCreate_TankDestroyedData(position);
    }

    public static void ProcessEndRound(int round, bool playerWin) {
        float playerDistance = Vector3.Distance(m_PlayerInitialPosition, m_PlayerEndPosition);
        float botDistance = Vector3.Distance(m_BotInitialPosition, m_BotEndPosition);
        SqliteManager.SQLCREATE_RoundData(m_PlayerShotsRound, m_BotShotsRound, playerDistance, botDistance, (double)(m_EndTime - m_StartTime), round);
        float accuracy = (float)m_PlayerHitsRound / (m_PlayerShotsRound == 0 ? 1 : m_PlayerShotsRound);
        SqliteManager.UpdateMongoDB(accuracy, playerWin);

        // Add shots to total and reset
        m_PlayerShotsGame += m_PlayerShotsRound;
        m_PlayerShotsRound = 0;
        m_PlayerHitsRound = 0;
        m_BotShotsGame += m_BotShotsRound;
        m_BotShotsRound = 0;
    }

    public static void ProcessEndGame() {
        SqliteManager.SQLCreate_ShotsFiredData(m_PlayerShotsGame, m_BotShotsGame);

        // Reset totals
        m_PlayerShotsGame = 0;
        m_BotShotsGame = 0;
    }

}
