﻿using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System;
using UnityEngine;
using System.Data;
using Mono.Data.Sqlite;
using MongoDB.Driver;
using MongoDB.Bson.Serialization.Attributes;

public class Highscore
{
    [BsonId]
    public int id { get; set; }
    public string Username { get; set; }
    public float Accuracy { get; set; }
    public int TotalRoundsWon { get; set; }
}

public static class SqliteManager {

    private static string databasePath = Application.dataPath + "/Tanks.db";

    // Used for dynamic creation of database
    public static void Initialize() {
        Debug.Log(databasePath);
        CreateTable_PositionDestroyed();
        CreateTable_RoundData();
        CreateTable_ShotsFired();
    }

    #region CreateTables

    private static void CreateTable_PositionDestroyed() {
        // Create the position destroyed table, but only if not already there
        using (SqliteConnection dbConnection = new SqliteConnection("Data Source=" + databasePath)) {
            dbConnection.Open();

            SqliteCommand createTableSQL = new SqliteCommand(
                "CREATE TABLE IF NOT EXISTS PositionDestroyed" +
                "(id INTEGER PRIMARY KEY UNIQUE, " +
                "PosX FLOAT NOT NULL, " +
                "PosY FLOAT NOT NULL, " +
                "PosZ FLOAT NOT NULL )",
                dbConnection);
            createTableSQL.ExecuteNonQuery();

            dbConnection.Close();
        }
    }

    private static void CreateTable_RoundData() {
        // Create the round data table, but only if not already there
        using (SqliteConnection dbConnection = new SqliteConnection("Data Source=" + databasePath)) {
            dbConnection.Open();

            SqliteCommand createTableSQL = new SqliteCommand(
                "CREATE TABLE IF NOT EXISTS RoundData" +
                "(id INTEGER PRIMARY KEY UNIQUE, " +
                "PlayerShots INTEGER NOT NULL, " +
                "BotShots INTEGER NOT NULL, " +
                "PlayerDistance FLOAT NOT NULL," +
                "BotDistance FLOAT NOT NULL," +
                "Time DOUBLE NOT NULL )",
                dbConnection);
            createTableSQL.ExecuteNonQuery();

            dbConnection.Close();
        }
    }

    private static void CreateTable_ShotsFired() {
        // Create the shots fired table, but only if not already there
        using (SqliteConnection dbConnection = new SqliteConnection("Data Source=" + databasePath)) {
            dbConnection.Open();

            SqliteCommand createTableSQL = new SqliteCommand(
                "CREATE TABLE IF NOT EXISTS ShotsFired" +
                "(id INTEGER PRIMARY KEY UNIQUE, " +
                "PlayerShots INTEGER NOT NULL, " +
                "BotShots INTEGER NOT NULL)",
                dbConnection);
            createTableSQL.ExecuteNonQuery();

            dbConnection.Close();
        }
    }

    #endregion

    #region CreateData

    public static void SQLCreate_TankDestroyedData(Vector3 position) {
        using (SqliteConnection dbConnection = new SqliteConnection("Data Source=" + databasePath)) {
            dbConnection.Open();

            SqliteCommand insertSQL = new SqliteCommand("INSERT INTO PositionDestroyed (PosX, PosY, PosZ) VALUES (@PosX, @PosY, @PosZ)", dbConnection);
            insertSQL.Parameters.AddWithValue("@PosX", position.x);
            insertSQL.Parameters.AddWithValue("@PosY", position.y);
            insertSQL.Parameters.AddWithValue("@PosZ", position.z);
            insertSQL.ExecuteNonQuery();

            dbConnection.Close();
        }
    }

    public static void SQLCREATE_RoundData(int playerShots, int botShots, float playerDistance, float botDistance, double time, int round) {
        using (SqliteConnection dbConnection = new SqliteConnection("Data Source=" + databasePath)) {
            dbConnection.Open();

            SqliteCommand insertSQL = new SqliteCommand("INSERT INTO RoundData (PlayerShots, BotShots, PlayerDistance, BotDistance, Time, Round)" +
                " VALUES (@PlayerShots, @BotShots, @PlayerDistance, @BotDistance, @Time, @Round)", dbConnection);
            insertSQL.Parameters.AddWithValue("@PlayerShots", playerShots);
            insertSQL.Parameters.AddWithValue("@BotShots", botShots);
            insertSQL.Parameters.AddWithValue("@PlayerDistance", playerDistance);
            insertSQL.Parameters.AddWithValue("@BotDistance", botDistance);
            insertSQL.Parameters.AddWithValue("@Time", time);
            insertSQL.Parameters.AddWithValue("@Round", round);
            insertSQL.ExecuteNonQuery();

            dbConnection.Close();
        }
    }

    public static void SQLCreate_ShotsFiredData(int playerShots, int botShots) {
        using (SqliteConnection dbConnection = new SqliteConnection("Data Source=" + databasePath)) {
            dbConnection.Open();

            SqliteCommand insertSQL = new SqliteCommand("INSERT INTO ShotsFired (PlayerShots, BotShots) VALUES (@PlayerShots, @BotShots)", dbConnection);
            insertSQL.Parameters.AddWithValue("@PlayerShots", playerShots);
            insertSQL.Parameters.AddWithValue("@BotShots", botShots);
            insertSQL.ExecuteNonQuery();

            dbConnection.Close();
        }
    }

    #endregion

    #region MongoDB

    public static void UpdateMongoDB(float accuracy, bool playerWin) {
        MongoClient client = new MongoClient("mongodb+srv://GDAP-Student:jous.trok4POOS_pood@gdap2020-cluster.swxsa.mongodb.net/<dbname>?retryWrites=true&w=majority");
        IMongoDatabase db = client.GetDatabase("GDAP_Exercise");
        Highscore highscore = db.GetCollection<Highscore>("Highscores").Find(u => u.Username == "Matthew Smith").Limit(1).Single();

        // Update accuracy only if better accuracy recorded
        if(highscore.Accuracy < accuracy) {
            highscore.Accuracy = accuracy;
        }

        // Update total wins if player won
        if (playerWin) {
            highscore.TotalRoundsWon += 1;
        }

        IMongoCollection<Highscore> coll = db.GetCollection<Highscore>("Highscores");
        coll.ReplaceOne(i => i.id == highscore.id, highscore);
    }


    #endregion

}
