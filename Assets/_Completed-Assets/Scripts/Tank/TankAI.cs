﻿using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using UnityEngine;
using UnityEngine.AI;

public class TankAI : MonoBehaviour
{
    // Simple AI for Tanks.
    public enum State
	{
        Waiting,
        Idle,
        Seek,
        Fire,
        Reload,
        Death
	}

    public State state = State.Waiting;

    public float seekDelay = 1.0f;
    private float currentSeekDelay = 0.0f;
    public float fireDistance = 5.0f;
    public float fireRate = 0.5f;
    public float currentFireRate = 0.5f;
    public int reloadCount = 5;
    public int currentFireCount = 0;
    public float reloadDelay = 1.0f;
    public float currentReloadDelay = 0.0f;

    private NavMeshPath path;
    private int pathIndex = 0;

    public float m_deadZone = 10f;                 // How fast the tank moves forward and back.
    public float angularDampeningTime = 5f;                 // How fast the tank moves forward and back.
    public float m_Speed = 12f;                 // How fast the tank moves forward and back.
    public float m_TurnSpeed = 180f;            // How fast the tank turns in degrees per second.
    private Rigidbody m_Rigidbody;              // Reference used to move the tank.

    private GameObject seekTank = null;

    public Rigidbody m_Shell;                   // Prefab of the shell.
    public Transform m_FireTransform;           // A child of the tank where the shells are spawned.
    public AudioSource m_ShootingAudio;         // Reference to the audio source used to play the shooting audio. NB: different to the movement audio source.
    public AudioClip m_FireClip;                // Audio that plays when each shot is fired.
    public float m_MinLaunchForce = 15f;        // The force given to the shell if the fire button is not held.
    public float m_MaxLaunchForce = 30f;        // The force given to the shell if the fire button is held for the max charge time.
    public float m_MaxChargeTime = 0.75f;       // How long the shell can charge for before it is fired at max force.
    private float m_ChargeSpeed;                // How fast the launch force increases, based on the max charge time.
    private float m_CurrentLaunchForce;         // The force that will be given to the shell when the fire button is released.

    private void Start()
	{
        path = new NavMeshPath();
        m_Rigidbody = GetComponent<Rigidbody> ();
        //agent = GetComponent<NavMeshAgent>();
    }

    public void Reset()
	{
	}

    public void EnableControl()
    {
        state = State.Idle;
    }

    public void DisableControl()
    {
        state = State.Waiting;
    }

    private void FixedUpdate()
    {
        switch(state)
		{
            case State.Waiting:
                break;

            case State.Idle:
                state = State.Seek;
                break;

            case State.Seek:
                seek();
                break;

            case State.Fire:
                fire();
                break;

            case State.Reload:
                reload();
                break;

            case State.Death:
                break;
		}
    }

    private void reload()
    {
        currentReloadDelay += Time.deltaTime;
        if (currentReloadDelay >= reloadDelay)
		{
            currentReloadDelay = 0.0f;
            state = State.Seek;
        }
    }

    private void fire()
	{
        Vector3 direction = (seekTank.transform.position - transform.position).normalized;
        direction.y = 0.0f;
        float angle = Vector3.Angle(transform.forward, direction);
        if (Mathf.Abs(angle) <= m_deadZone)
        {
            transform.LookAt(transform.position + direction);
        }
        else
        {
            transform.rotation = Quaternion.Lerp(transform.rotation,
                                                 Quaternion.LookRotation(direction),
                                                 Time.deltaTime * angularDampeningTime);
        }

        currentFireRate += Time.deltaTime;
        if (currentFireRate >= fireRate)
		{
            // Record shot fired
            DataManager.ShotFired(false); // Player uses TankShooting for firing

            currentFireRate = 0.0f;

            Rigidbody shellInstance = Instantiate(m_Shell, m_FireTransform.position, m_FireTransform.rotation) as Rigidbody;

            // Set the shell's velocity to the launch force in the fire position's forward direction.
            shellInstance.velocity = m_CurrentLaunchForce * m_FireTransform.forward;

            // Change the clip to the firing clip and play it.
            m_ShootingAudio.clip = m_FireClip;
            m_ShootingAudio.Play();

            // Reset the launch force.  This is a precaution in case of missing button events.
            m_CurrentLaunchForce = m_MinLaunchForce;
    
            currentFireCount++;
            if (currentFireCount >= reloadCount)
            {
                currentFireCount = 0;
                state = State.Reload;
            }
        }

        if ((seekTank.transform.position - transform.position).magnitude > fireDistance)
        {
            state = State.Seek;
            return;
        }
    }

    private void seek()
	{
        currentSeekDelay += Time.deltaTime;
        if (currentSeekDelay >= seekDelay)
		{
            currentSeekDelay = 0.0f;
            float distance = float.MaxValue;
            seekTank = null;
            foreach (var tank in Complete.GameManager.gameManager.m_Tanks)
            {
                if (tank.m_AI != this)
                {
                    float dis = (tank.m_Instance.transform.position - gameObject.transform.position).sqrMagnitude;
                    if (dis < distance)
                    {
                        distance = dis;
                        seekTank = tank.m_Instance;
                    }
                }
            }
            NavMesh.CalculatePath(transform.position, seekTank.transform.position, NavMesh.AllAreas, path);
            pathIndex = 0;
        }

        if (pathIndex < path.corners.Length - 1)
		{
            if ((seekTank.transform.position - transform.position).magnitude <= fireDistance)
            {
                state = State.Fire;
                return;
            }
            Vector3 direction = (path.corners[pathIndex + 1] - transform.position).normalized;
            Vector3 movement = direction * m_Speed * Time.deltaTime;
            movement.y = 0.0f;
            m_Rigidbody.MovePosition(m_Rigidbody.position + movement);

            direction.y = 0.0f;
            float angle = Vector3.Angle(transform.forward, direction);
            if (Mathf.Abs(angle) <= m_deadZone)
            {
                transform.LookAt(transform.position + direction);
            }
            else
            {
                transform.rotation = Quaternion.Lerp(transform.rotation,
                                                     Quaternion.LookRotation(direction),
                                                     Time.deltaTime * angularDampeningTime);
            }
        }
    }
}
